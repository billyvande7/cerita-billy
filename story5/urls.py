from django.urls import path
from . import views

# Create your views here.


app_name = 'story5'

urlpatterns = [
    path('', views.story5, name='story5'),
    path('<int:id>/', views.detail_matkul, name='detail'),
]

