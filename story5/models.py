from django.db import models
from django.utils import timezone
from datetime import date, datetime

# Create your models here.


class Jadwal(models.Model):
    nama_matkul = models.CharField(max_length=50)
    nama_dosen = models.CharField(max_length=50)
    kelas = models.CharField(max_length=3, default="")
    semester_berapa = models.IntegerField(default=3)
    deskripsi = models.TextField(default="")

    def __str__(self):
        return self.nama_matkul