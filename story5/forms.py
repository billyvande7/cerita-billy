from django import forms
from django.db.models import fields
from .models import Jadwal

# Create your models here.


class JadwalForm(forms.ModelForm):
    class Meta():
        model = Jadwal
        fields = "__all__"
        widgets = {
            'nama_matkul' : forms.TextInput(attrs={'placeholder' : 'PPW'}),
            'nama_dosen' : forms.TextInput(attrs={'placeholder' : 'Iis'}),
            'kelas' : forms.TextInput(attrs={'placeholder' : 'C'}),
            'semester_berapa' : forms.TextInput(attrs={'placeholder' : '3'}),
            'deskripsi' : forms.Textarea(attrs={'placeholder' : 'omoshiroi'}),
        }
        labels = {
            'nama_matkul' : "Nama Mata Kuliah",
            'nama_dosen' : "Nama Dosen",
            'kelas' : "Kelas",
            'semester_berapa' : "Semester",
            'deskripsi' : "Deskripsi",
        }