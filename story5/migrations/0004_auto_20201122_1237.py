# Generated by Django 3.1.1 on 2020-11-22 05:37

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('story5', '0003_auto_20201122_1225'),
    ]

    operations = [
        migrations.AlterField(
            model_name='jadwal',
            name='kelas',
            field=models.CharField(default='', max_length=3),
        ),
        migrations.AlterField(
            model_name='jadwal',
            name='nama_dosen',
            field=models.CharField(max_length=20),
        ),
        migrations.AlterField(
            model_name='jadwal',
            name='nama_matkul',
            field=models.CharField(max_length=20),
        ),
    ]
