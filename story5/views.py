from django.shortcuts import redirect, render
from django.http import HttpResponse, HttpResponseRedirect, request, response
from .models import Jadwal
from .forms import JadwalForm



def story5(request):
    form = JadwalForm(request.POST or None)
    jadwal = Jadwal.objects.all()
    if form.is_valid():
        form.save()
        return redirect('/story5/')
    response = {
        'form': form,
        'jadwal': jadwal,
    }
    return render(request, 'story5/story5.html', response)


def detail_matkul(request, id):
    matkul = Jadwal.objects.get(id=id)
    response = {
        'matkul': matkul
    }
    return render(request, 'story5/matkul.html', response)
