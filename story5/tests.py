from django.test import TestCase
from .models import Jadwal

# Create your tests here.

class TestApp(TestCase):    
    def test_model_print_as_name(self):
        jadwal = Jadwal.objects.create(nama_matkul = 'ppw', nama_dosen = 'iis')
        self.assertEquals(str(jadwal),jadwal.nama_matkul)