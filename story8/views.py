from django.http import response, JsonResponse
from django.shortcuts import render
import requests
import json

# Create your views here.

def story8(request):
    return render(request, 'story8/story8.html')

def storysearch(request):
    args = request.GET['q']
    url = 'https://www.googleapis.com/books/v1/volumes?q=' + args
    r = requests.get(url)
    data = json.loads(r.content)
    return JsonResponse(data , safe=False)