from django.test import TestCase, Client
from django.urls import resolve
from django.apps import apps

from .apps import Story8Config
from .views import story8, storysearch

# Create your tests here.

class TestApp(TestCase):    
    def test_is_app_available(self):
        self.assertEqual(Story8Config.name, 'story8')
        self.assertEqual(apps.get_app_config(
            'story8').name, 'story8')

    def test_url_is_exist(self):
        response = Client().get('/story8/')
        self.assertEquals(response.status_code, 200)
        response = Client().get('/story8/cari?q=')
        self.assertEquals(response.status_code, 301)

    def test_is_detail_page_exists(self):
        response = self.client.post('/story8/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'story8/story8.html', 'base.html')
        found = resolve('/story8/')
        self.assertEqual(found.func, story8)