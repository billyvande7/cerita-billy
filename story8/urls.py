from django.urls import path
from . import views

# Create your views here.


app_name = 'story8'

urlpatterns = [
    path('', views.story8, name='story8'),
    path('cari/', views.storysearch, name='storysearch'),
]
