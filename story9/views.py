from django.contrib import messages
from django.contrib.auth import login as auth_login, authenticate, logout as auth_logout
from django.contrib.auth.decorators import login_required
from django.contrib.auth.forms import UserCreationForm
from django.http.response import HttpResponse
from django.shortcuts import render, redirect

# Create your views here.

def story9(request):
    return render(request, 'story9/story9.html')

# def login(request):
#     if(request.method == 'POST'):
#         username = request.POST['username']
#         password = request.POST['password']
#         user = authenticate(request, username=username, password=password)
#     if user is not None:
#         auth_login(request, user)
#         return redirect('/story9/')
#     else:
#         return render(request, 'story9/signin.html')

def signup(request):
    if request.method == 'POST':
        form = UserCreationForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            raw_password = form.cleaned_data.get('password1')
            user = authenticate(username=username, password=raw_password)
            auth_login(request , user)
            return redirect("/")
    else:
        form = UserCreationForm()
    return render(request, 'story9/signup.html', {'form': form})

@login_required
def logout(request):
    # if user.is_superuser:
    #     return redirect('/admin/logout/?next=/')
    auth_logout(request)
    return redirect('/story9/')