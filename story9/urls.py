from django.urls import path
from . import views
from django.contrib.auth import login, views as auth_views

# Create your views here.


app_name = 'story9'

urlpatterns = [
    path('', views.story9, name='story9'),
    # path('signin/', views.signin, name='signin'),
    path('signup/', views.signup, name='signup'),
    path('logout/', views.logout, name='logout'),
    path('login/', auth_views.LoginView.as_view(), name = 'login'),
    
]
