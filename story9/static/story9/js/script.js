$(document).ready(function(){
    $("#tombol").click( function (){
        var isi = $("#cari").val();
        var url_search = "/story8/cari?q=" + isi ;

        $.ajax({
            url : url_search,
            success : function(hasil){
                var objects = $('#hasil');
                var objek = '';
                objects.empty();
                objects.append('<tr><td>nama buku</td><td>author</td><td>publish date</td><td>gambar</td></tr>')
                for ( i = 0; i < hasil.items.length; i++) {
                    var title , author , published_date , thumbnail ;
                    title = hasil.items[i].volumeInfo.title;
                    author = hasil.items[i].volumeInfo.authors;
                    published_date = hasil.items[i].volumeInfo.publishedDate;
                    thumbnail = hasil.items[i].volumeInfo.imageLinks.smallThumbnail;
                    if (author == undefined) {
                        author = "Unknown";
                    }
                    if (published_date == undefined) {
                        published_date = "Unknown";
                    }
                    objects.append('<tr><td>'+ title + '</td> /n <td>'+ author + 
                    '</td> /n <td>' + published_date + '</td> /n <td><img src=' + thumbnail + '</td> </tr>' );
                }
                
            }

        })
    })
});