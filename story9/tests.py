from django.http import response
from django.test import TestCase, Client
from django.urls import resolve
from django.apps import apps
from django.contrib.auth.models import User

from .apps import Story9Config

# Create your tests here.

class TestApp(TestCase):    
    def test_is_app_available(self):
        self.assertEqual(Story9Config.name, 'story9')
        self.assertEqual(apps.get_app_config('story9').name, 'story9')
    
    def test_url_is_exist(self):
        response = Client().get('/story9/')
        self.assertEquals(response.status_code, 200)
        response = Client().get('/story9/login/')
        self.assertEquals(response.status_code, 200)
        response = Client().get('/story9/signup/')
        self.assertEquals(response.status_code, 200)

    # def can_save_POST(self):
    #     response = self.client.post('/story9/signup/', data = {
    #         'username':'asalaja',
    #         'password' : 'lebihasal12'
    #     })
    #     user_count = User.objects.all().count()
    #     self.assertEquals(user_count,1)

    # def test_logout(self):
    #     # Log in
    #     self.client.login(username='XXX', password="XXX")

    #     # Check response code
    #     response = self.client.get('/admin/')
    #     self.assertEquals(response.status_code, 302)

    #     self.client.get('/admin/')
    #     # Log out
    #     self.client.logout()

    #     # Check response code
    #     response = self.client.get('/admin/')
    #     self.assertEquals(response.status_code, 302)