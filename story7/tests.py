from django.test import TestCase, Client
from django.urls import resolve

# Create your tests here.

class TestApp(TestCase):    
    def test_url_is_exist(self):
        response = Client().get('/story7/')
        self.assertEquals(response.status_code, 200)

    def test_using_template(self):
        response = Client().get('/story7/')
        self.assertTemplateUsed(response, 'story7/story7.html', 'base.html')

    def test_using_funct(self):
        found = resolve('/story7/')
        self.assertEquals(found.func.__name__, 'story7')

    