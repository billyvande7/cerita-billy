from django.urls import path
from . import views

# Create your views here.


app_name = 'story7'

urlpatterns = [
    path('', views.story7, name='story7'),
]
