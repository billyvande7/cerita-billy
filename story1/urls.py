from django.urls import path
from . import views

# Create your views here.


app_name = 'story1'

urlpatterns = [
    path('', views.story1, name='story1'),
]
