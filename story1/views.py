from django.shortcuts import redirect, render
from django.http import HttpResponse, HttpResponseRedirect, request, response


def story1(request):
    return render(request, 'story1/story1.html')
