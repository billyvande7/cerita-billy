from django import forms
from django.db.models import fields
# from .models import Absen, Jadwal, Kegiatan

# Create your models here.


# class JadwalForm(forms.Form):
#     nama_matkul = forms.CharField()
#     nama_dosen = forms.CharField()
#     semester_berapa = forms.IntegerField()


# class JadwalForm(forms.ModelForm):
#     class Meta:
#         model = Jadwal
#         fields = '__all__'

#     error_messages = {
#         'required': 'please type'
#     }

#     input_attrs = {
#         'type': 'text',
#         'placeholder': 'nama matkul'
#     }

#     nama_matkul = forms.CharField(widget=forms.TextInput(attrs=input_attrs))
#     nama_dosen = forms.CharField(required=True)
#     semester_berapa = forms.IntegerField(required=True, max_value=12)


# class AbsenForm(forms.ModelForm):
#     class Meta:
#         model = Absen
#         fields = '__all__'

#     error_messages = {
#         'required': 'please type'
#     }

#     input_attrs1 = {
#         'type': 'text',
#         'placeholder': 'Name goes Here'
#     }
#     nama = forms.CharField(widget=forms.TextInput(attrs=input_attrs1))


# class KegiatanForm(forms.ModelForm):
#     class Meta:
#         model = Kegiatan
#         fields = '__all__'

#     error_messages = {
#         'required': 'please type'
#     }

#     input_attrs = {
#         'type': 'text',
#         'placeholder': 'kegiatan goes here'
#     }
#     kegiatan = forms.CharField(widget=forms.TextInput(attrs=input_attrs))
