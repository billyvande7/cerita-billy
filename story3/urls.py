from django.urls import path
from . import views

# Create your views here.


app_name = 'story3'

urlpatterns = [
    path('', views.story3, name='story3'),
]
