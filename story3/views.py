from django.shortcuts import redirect, render
from django.http import HttpResponse, HttpResponseRedirect, request, response


def story3(request):
    return render(request, 'story3/story3.html')
