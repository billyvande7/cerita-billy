from django.http import response, HttpResponseRedirect
from django.shortcuts import render, redirect

from .models import Absen , Kegiatan
from .forms import AbsenForm
# Create your views here.


def story6(request):
    formAbsen = AbsenForm(request.POST or None)
    absen = Absen.objects.all()
    kegiatan = Kegiatan.objects.all()
    if formAbsen.is_valid():
        formAbsen.save()
        return redirect('/story6/')
    response = {
        'formAbsen': formAbsen,
        'absen': absen,
        'kegiatan':kegiatan,
    }
    return render(request, 'story6/story6.html', response)