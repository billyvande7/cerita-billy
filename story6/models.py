from django.db import models

# Create your models here.

class Kegiatan(models.Model):
    kegiatan = models.TextField(max_length=30)

    def __str__(self):
        return self.kegiatan

class Absen(models.Model):
    kegiatan = models.ForeignKey(Kegiatan, on_delete=models.SET_NULL, blank=True, null=True)
    nama = models.TextField(max_length=30)
    waktu_absen = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.nama