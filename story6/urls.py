from django.urls import path
from . import views

# Create your views here.


app_name = 'story6'

urlpatterns = [
    path('', views.story6, name='story6'),
]
