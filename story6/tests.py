from django.http import response
from django.test import TestCase, Client
from django.urls import resolve
from .models import Absen, Kegiatan
from .views import story6


# Create your tests here.

class TestApp(TestCase):
    def test_url_is_exist(self):
        response = Client().get('/story6/')
        self.assertEquals(response.status_code, 200)

    def test_using_template(self):
        response = Client().get('/story6/')
        self.assertTemplateUsed(response, 'story6/story6.html', 'base.html')

    def test_using_funct(self):
        found = resolve('/story6/')
        self.assertEquals(found.func.__name__, 'story6')

    def test_model_active(self):
        test_jadwal = Kegiatan.objects.create()
        count_jadwal = Kegiatan.objects.all().count()
        self.assertEquals(count_jadwal, 1)

        test_absen = Absen.objects.create()
        count_absen = Absen.objects.all().count()
        self.assertEquals(count_jadwal, 1)

    def test_model_print_as_name(self):
        giat = Kegiatan.objects.create(kegiatan = 'memancing')
        self.assertEquals(str(giat) ,giat.kegiatan)
        absen = Absen.objects.create(kegiatan = giat , nama = 'kevin')
        self.assertEquals(str(absen),absen.nama)


#     def test_save_post(self):
#         self.assertEquals(response.status_code, 200)
#         self.assertEquals(response.get('location'), None)

# new_response = self.client.get('/story5/')
# html_response = new_response.content.decode('utf8')
# self.assertIn('apa ajalah', html_response)
