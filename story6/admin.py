from django.contrib import admin

from .models import Absen , Kegiatan

# Register your models here.

class AbsenInLine(admin.StackedInline):
    model = Absen


class KegiatanAdmin(admin.ModelAdmin):
    inlines = [AbsenInLine, ]

admin.site.register(Kegiatan, KegiatanAdmin)