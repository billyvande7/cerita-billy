from django import forms
from django.db.models import fields
from .models import Absen

# Create your models here.


class AbsenForm(forms.ModelForm):
    class Meta():
        model = Absen
        fields = "__all__"
        widgets = {
            # 'nama_matkul' : forms.TextInput(attrs={'placeholder' : 'PPW'}),
            'nama' : forms.TextInput(attrs={'placeholder' : 'Billy'}),
        }
        labels = {
            'kegiatan' : "Jenis Kegiatan",
            'nama' : "Nama Kamu",
        }